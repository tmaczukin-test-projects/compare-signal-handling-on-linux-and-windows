export CGO_ENABLED := 0

compare-signal-handling-*:
	gox \
		-ldflags "-s -w" \
		-osarch linux/amd64 \
		-osarch windows/amd64 \
		-output="compare-signal-handling-{{.OS}}-{{.Arch}}" \
		.

.PHONY: upload
upload: compare-signal-handling-*
	minio-client cp compare-signal-handling-* artifacts-maczukin/compare-signal-handling-on-linux-and-windows/

.PHONY: clean
clean:
	rm compare-signal-handling-*


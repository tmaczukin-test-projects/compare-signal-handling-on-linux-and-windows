# Compare signal handling on Linux and Windows

## Downloads

- Linux: https://artifacts.maczukin.pl/compare-signal-handling-on-linux-and-windows/compare-signal-handling-linux-amd64
- Windows: https://artifacts.maczukin.pl/compare-signal-handling-on-linux-and-windows/compare-signal-handling-windows-amd64.exe

## LICENSE

MIT

## Author

Tomasz Maczukin, 2019


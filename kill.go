// +build !windows

package main

import (
	"syscall"
)

func killPid(pid int, signal syscall.Signal) error {
	return syscall.Kill(pid, signal)
}

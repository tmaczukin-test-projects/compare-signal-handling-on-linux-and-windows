package main

import (
	"bytes"
	"fmt"
	"os"
	"os/exec"
	"os/signal"
	"strconv"
	"syscall"
	"time"
)

type command func(args []string)

var self string

func main() {
	fmt.Printf("Started new process: pid=%d\n", os.Getpid())

	commands := map[string]command{
		"start":    start,
		"kill":     kill,
		"normal":   normal,
		"selftest": selftest,
	}

	cmd := os.Args[1]
	command, ok := commands[cmd]
	if !ok {
		panic(fmt.Sprintf("Uknown command %q", cmd))
	}

	self = os.Args[0]

	var args []string
	if len(os.Args) > 2 {
		args = os.Args[2:]
	}

	command(args)
}

func start(args []string) {
	closeCh := make(chan os.Signal)
	signal.Notify(closeCh, syscall.SIGHUP, syscall.SIGINT, syscall.SIGTERM)

	for {
		select {
		case sig := <-closeCh:
			fmt.Printf("\nReceived signal: %#v", sig)
			return

		case <-time.After(1 * time.Second):
			fmt.Print(".")
		}
	}
}

func kill(args []string) {
	sig := getSignal(args[0])

	pid, err := strconv.Atoi(args[1])
	if err != nil {
		panic(fmt.Sprintf("Error on pid parsing: %v", err))
	}

	err = killPid(pid, sig)
	if err != nil {
		panic(fmt.Sprintf("Error when sending signal %v to pid %d: %v", sig, pid, err))
	}
}

func getSignal(sig string) syscall.Signal {
	signals := map[string]syscall.Signal{
		"SIGHUP":  syscall.SIGHUP,
		"SIGINT":  syscall.SIGINT,
		"SIGTERM": syscall.SIGTERM,
	}

	ssig, ok := signals[sig]
	if !ok {
		panic(fmt.Sprintf("Unsupported signal %q", sig))
	}

	return ssig
}

func normal(args []string) {
	for i := 0; i < 120; i++ {
		fmt.Print(".")
		time.Sleep(1 * time.Second)
	}
}

func selftest(args []string) {
	doSelftest(args[0], "start")
	doSelftest(args[0], "normal")
}

func doSelftest(s string, strategy string) {
	fmt.Printf("\nTesting %q sig handling with %q strategy\n", s, strategy)

	sig := getSignal(s)

	buf := bytes.NewBuffer(nil)

	cmd := exec.Command(self, strategy)
	cmd.Stdout = buf
	cmd.Stderr = buf

	err := cmd.Start()
	if err != nil {
		panic(fmt.Sprintf("Error while starting the command: %v", err))
	}

	waitCh := make(chan error)
	go func() {
		waitCh <- cmd.Wait()
	}()

	time.Sleep(2 * time.Second)

	err = cmd.Process.Signal(sig)
	if err != nil {
		panic(fmt.Sprintf("Error while sending signal %q to the command: %v", sig, err))
	}

	select {
	case err = <-waitCh:
		fmt.Printf("Received %#v from command\n", err)

	case <-time.After(5 * time.Second):
		fmt.Println("Forcing 5s timeout")
	}

	fmt.Println("buff:")
	fmt.Println(">>>>>>>>>>")
	fmt.Println(buf.String())
	fmt.Println("<<<<<<<<<<")
	fmt.Println("TEST FINISHED")
}
